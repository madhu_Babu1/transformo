
package com.mydevapps.transformo.WalkandStep;

import android.content.pm.PackageManager;

import com.mydevapps.transformo.WalkandStep.services.AbstractStepDetectorService;
import com.mydevapps.transformo.WalkandStep.services.AccelerometerStepDetectorService;
import com.mydevapps.transformo.WalkandStep.services.HardwareStepDetectorService;
import com.mydevapps.transformo.WalkandStep.utils.AndroidVersionHelper;




public class Factory {



    public static Class<? extends AbstractStepDetectorService> getStepDetectorServiceClass(PackageManager pm){
        if(pm != null && AndroidVersionHelper.supportsStepDetector(pm)) {
            return HardwareStepDetectorService.class;
        }else{
            return AccelerometerStepDetectorService.class;
        }
    }
}
